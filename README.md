# สถานีตรวจวัดอากาศ
Dashboard ไว้สำหรับแสดงผลข้อมูลจาก Arduino 
ประกอบไปด้วย GAS SMOKE PM1 PM2.5 PM10 Humidity Temperature
***
![alt text](WebAMS/public/images/dashboard.png)
***
## ไฟล์
```
└── ams
    ├── ams.ino
    ├── MQ2.cpp
    ├── MQ2.h
    ├── MQ9.cpp
    ├── MQ9.h
    ├── PMS.cpp
    └── PMS.h
```
## การทำงาน
![alt text](WebAMS/public/images/diagram.PNG)
1. Arduino Mega รับข้อมูลจากเซนเซอร์ 
2. ส่งข้อมูลไปแสดงผลที่ Dashboard
## อุปกรณ์ที่ต้องใช้
- Arduino Mega 2560 x1
- 3G Shield (UC20-G) x1
- Particle sensor PMS7003 x1
- Humidity and Temperature sensor DHT11 x1
- Smoke sensor MQ2 x1
- Gas sensor MQ9 x1
- Switching Power supply 5V7A x1
- สาย DC Jack 5.5 X 2.5 mm x1
- สาย AC cable x1
- Jumper Male to Female x6
- Jumper Male to Male x6
- 3G Rubber Antenna x1
- Breadboard Pin Adapter สำหรับ PM2.5 Sensor Module (PMS7003) x1


## โปรแกรมที่ใช้งาน
- Arduino IDE
- MQTTBox สำหรับแสดงผลข้อมูลที่ Arduino ส่งมา

### ขั้นตอนที่ 1 : การต่ออุปกรณ์กับ 3G Shield (UC20-G)
เริ่มจากการถอด Jumper ที่ AutoStart และ SoftwareSerial Pin(SW) และนำ Jumper ไปใส่ที่ SoftwareStart Pin เพื่อที่สั่ง PowerOn ภายในโค้ดและจะติดต่อกับ Arduino ผ่าน HardwareSerial จากนั้นใส่เสา 3G Rubber Antenna ไปที่ช่อง MAIN และต่อ 3G Shield เข้ากับ Arduino และต่อสาย Jumper จาก HW Pin เข้า 19,18 ของ Arduino ตามรูปด้านล่าง
![alt text](WebAMS/public/images/3g.PNG) 
รูปภาพจาก https://www.thaieasyelec.com/downloads/ETEE059/Development_Guide_for_3G_Shield_and_3G_Module_using_Arduino_TH_20160726.pdf 

### ขั้นตอนที่ 2 : ต่อ Switching power supply
ต่อ DC Jack 5.5 X 2.5 mm เข้ากับ Switching power supply โดยให้สายสีแดง,ดำเข้ากับ +V,-V ตามลำดับ และต่อสาย AC cable เข้าที่ L,N
![alt text](WebAMS/public/images/22291.jpg)


### ขั้นตอนที่ 3 : การต่อเซนเซอร์กับอุปกรณ์
นำเซนเซอร์ MQ9, MQ2 ,PMS7003 และ DHT11 ต่อเข้ากับ Arduino โดยต่อเข้า pin ดังนี้
```
MQ9 -> Arduino
Vcc -> 5V
Gnd -> Gnd
AD -> A3

MQ2 -> Arduino
Vcc -> 5V
Gnd -> Gnd
AD -> A11

PMS -> Arduino
Vcc -> 5V
Gnd -> Gnd
Tx -> 17(Rx)
Rx -> 16(Tx)

DHT11 -> Arduino
Vcc -> 5V
Gnd -> Gnd
Signal(Data) -> 8
```
![alt text](WebAMS/public/images/all.jpg)


### ขั้นตอนที่ 4 : Download code และ กำหนดค่าเบื้องต้น
ให้ทำการ clone repository นี้โดยใช้คำสั่ง 
```
git clone https://gitlab.com/supavit13/ams
```
จากนั้นกำหนดค่าเบื้องต้นในไฟล์ `/ams/ams.ino`
- แก้ไข APN ของเครือข่ายที่ใช้งาน ในที่นี้จะยกตัวอย่าง 4 เครือข่าย ดังนี้

AIS
```
#define APN "internet"
#define USER " "
#define PASS " "
```
Truemove
```
#define APN "internet"
#define USER "True"
#define PASS "true"
```
DTAC
```
#define APN "www.dtac.co.th"
#define USER " "
#define PASS " "
```
TOT
```
#define APN "internet"
#define USER " "
#define PASS " "
```

ข้อมูล APN จาก https://www.thaieasyelec.com/downloads/ETEE059/ETEE059_3G_Shield_User_Manual_TH_20160321.pdf'

กำหนดข้อมูลอุปกรณ์ดังนี้
```
#define ALIAS "soar" //<--- ชื่ออุปกรณ์
#define PLACE "Space Krenovation Park" //<--- สถานที่ติดตั้งอุปกรณ์
String location = "13.102115,100.927842"; //<---- พิกัด latitude,longitude
```

### ขั้นตอนที่ 5 : ติดตั้ง Library และ Upload program to arduino

ต่อสาย USB กับ Arduino เข้าคอมฯ คัดลอกแฟ้ม `TEE_UC20_Shield` ไปที่ `C:\Users\username\Documents\Arduino\libraries` จากนั้นเข้าไปที่แฟ้ม ams เปิด ams.ino ด้วย Arduino IDE และเลือก Port, Processor, Board และ Programmer ให้ตรงกับบอร์ดที่ใช้งาน 
![alt text](WebAMS/public/images/port.png "การเลือก Port, Processor, Board และ Programmer") 
Port สามารถดูได้จาก Device manager จากนั้นกด Upload แล้วรอจนกว่าจะขึ้นว่า `Done uploading`เป็นอันเสร็จขั้นตอนนี้
![alt text](WebAMS/public/images/done.png "Done uploading")

### ทดลองอ่าน Serial Monitor
ต่อ DC Jack 5.5 X 2.5 mm และ USB เข้ากับ Arduino จากนั้นคลิกที่ Serial Monitor เพื่อดูการทำงาน ถ้าแสดงผลเหมือนภาพด้านล่าง ถือว่าเป็นอันเสร็จสมบูรณ์

![alt text](WebAMS/public/images/debug.PNG )

### ทดสอบอ่านข้อมูลจาก MQTTBox
โปรแกรม MQTTBox เป็นตัวติดตามและส่งข้อมูลผ่าน Protocol MQTT ในส่วนนี้จะอธิบายวิธีติดตามข้อมูลจาก Topic ชื่อ ams
ตัวโปรแกรมสามารถดาวน์โหลดได้ตามลิ้งนี http://workswithweb.com/html/mqttbox/downloads.html
หลังจากติดตั้งโปรแกรมเรียบร้อยแล้วให้เปิดโปรแกรมและทำการ Create MQTT Client จากนั้นแก้ไขข้อมูลต่อไปนี้
```
MQTT Client Name = MQTT SOAR
Protocol = mqtt / tcp
Host = soar.gistda.or.th:1883
```
![alt text](WebAMS/public/images/mqttconfig.png )

เมื่อแก้ไขเสร็จเรียบร้อยแล้วให้กด Save จะเข้าสู่หน้าของการติดตามและส่งข้อมูล ให้กรอก ams ในช่อง Topic to subscribe แล้วกด Subscribe เพื่อเป็นการเริ่มติดตามข้อมูลจาก Topic ชื่อ ams
![alt text](WebAMS/public/images/mqttsub.png )
หลังจากนั้นจะเห็นข้อมูลที่ Arduino ส่งมา 9 ตัวขั้นด้วยเครื่องหมาย `,` ซึ่งข้อมูลที่แสดงหมายถึงค่าที่ได้จากเซนเซอร์และข้อมูลอุปกรณ์ `MQ9, MQ2, PM1, PM2.5, PM10, Latitude, Longitude, Alias, Place, Humidity, Temperature` ตามลำดับซึ่งค่าเหล่านี้จะมีหน่วยเป็น `analog number, analog number, µg/m3, µg/m3, µg/m3, - , - , percent, celsius` ตามลำดับ
![alt text](WebAMS/public/images/mqttvalue.png )
