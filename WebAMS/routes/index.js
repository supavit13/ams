var express = require('express');
var router = express.Router();
var mqtt = require('mqtt')
var axios = require('axios')
var client = mqtt.connect('mqtt://soar.gistda.or.th')
var schemaData = {
  gas: 0,
  smoke: 0,
  pm1: 0,
  pm2_5: 0,
  pm10: 0,
  latlon: [0, 0],
  alias: "",
  place: "",
  pm2_5_api: 0,
  humidity: 0,
  temperature: 0
}
var data = schemaData
client.on('connect', function () {
  client.subscribe('ams', function (err) {
  })
})


client.on('message', function (topic, raw) {
  var msg = raw.toString()
  var value = msg.split(',')
  if (value.length > 2 && topic == "ams") {
    data.gas = parseFloat(value[0])
    data.smoke = parseFloat(value[1])
    data.pm1 = parseInt(value[2])
    data.pm2_5 = parseInt(value[3])
    data.pm10 = parseInt(value[4])
    data.latlon[0] = parseFloat(value[5])
    data.latlon[1] = parseFloat(value[6])
    data.alias = value[7]
    data.place = value[8]
    data.pm2_5_aqi = AQIPM25(value[3])
    data.humidity = parseFloat(value[9])
    data.temperature = parseFloat(value[10])
    if(data.alias == "soar"){
      data.latlon[0] = 18.590224
      data.latlon[1] = 98.487061
    }
  }
})
function InvLinear(AQIhigh, AQIlow, Conchigh, Conclow, a) {
  var AQIhigh;
  var AQIlow;
  var Conchigh;
  var Conclow;
  var a;
  var c;
  c = ((a - AQIlow) / (AQIhigh - AQIlow)) * (Conchigh - Conclow) + Conclow;
  return c;
}
function isnan(value) {
  return (value != undefined || value != null) ? value.v : 0
}
function ConcPM25(a) {
  if (a >= 0 && a <= 50) {
    ConcCalc = InvLinear(50, 0, 12, 0, a);
  }
  else if (a > 50 && a <= 100) {
    ConcCalc = InvLinear(100, 51, 35.4, 12.1, a);
  }
  else if (a > 100 && a <= 150) {
    ConcCalc = InvLinear(150, 101, 55.4, 35.5, a);
  }
  else if (a > 150 && a <= 200) {
    ConcCalc = InvLinear(200, 151, 150.4, 55.5, a);
  }
  else if (a > 200 && a <= 300) {
    ConcCalc = InvLinear(300, 201, 250.4, 150.5, a);
  }
  else if (a > 300 && a <= 400) {
    ConcCalc = InvLinear(400, 301, 350.4, 250.5, a);
  }
  else if (a > 400 && a <= 500) {
    ConcCalc = InvLinear(500, 401, 500.4, 350.5, a);
  }
  else {
    ConcCalc = "PM25message";
  }
  return parseInt(ConcCalc);
}
function ConcPM10(a) {
  if (a >= 0 && a <= 50) {
    ConcCalc = InvLinear(50, 0, 54, 0, a);
  }
  else if (a > 50 && a <= 100) {
    ConcCalc = InvLinear(100, 51, 154, 55, a);
  }
  else if (a > 100 && a <= 150) {
    ConcCalc = InvLinear(150, 101, 254, 155, a);
  }
  else if (a > 150 && a <= 200) {
    ConcCalc = InvLinear(200, 151, 354, 255, a);
  }
  else if (a > 200 && a <= 300) {
    ConcCalc = InvLinear(300, 201, 424, 355, a);
  }
  else if (a > 300 && a <= 400) {
    ConcCalc = InvLinear(400, 301, 504, 425, a);
  }
  else if (a > 400 && a <= 500) {
    ConcCalc = InvLinear(500, 401, 604, 505, a);
  }
  else {
    ConcCalc = "PM10message";
  }
  return parseInt(ConcCalc);
}
function Linear(AQIhigh, AQIlow, Conchigh, Conclow, Concentration) {
  var linear;
  var Conc = parseFloat(Concentration);
  var a;
  a = ((Conc - Conclow) / (Conchigh - Conclow)) * (AQIhigh - AQIlow) + AQIlow;
  linear = Math.round(a);
  return linear;
}
function AQIPM25(Concentration) {
  var Conc = parseFloat(Concentration);
  var c;
  var AQI;
  c = (Math.floor(10 * Conc)) / 10;
  if (c >= 0 && c < 12.1) {
    AQI = Linear(50, 0, 12, 0, c);
  }
  else if (c >= 12.1 && c < 35.5) {
    AQI = Linear(100, 51, 35.4, 12.1, c);
  }
  else if (c >= 35.5 && c < 55.5) {
    AQI = Linear(150, 101, 55.4, 35.5, c);
  }
  else if (c >= 55.5 && c < 150.5) {
    AQI = Linear(200, 151, 150.4, 55.5, c);
  }
  else if (c >= 150.5 && c < 250.5) {
    AQI = Linear(300, 201, 250.4, 150.5, c);
  }
  else if (c >= 250.5 && c < 350.5) {
    AQI = Linear(400, 301, 350.4, 250.5, c);
  }
  else if (c >= 350.5 && c < 500.5) {
    AQI = Linear(500, 401, 500.4, 350.5, c);
  }
  else {
    AQI = "PM25message";
  }
  return AQI;
}
setInterval(function () {

}, 1000)

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Air Measuring Station' });
});
router.get('/data', function (req, res, next) {
  res.json(data);
});
router.get('/data/waqi', function (req, res) {
  
  axios.all([
    axios.get('https://api.waqi.info/feed/geo:18.817051;98.297568/?token=7fc8bbce26b91a5d2746cf445ae2920ee3ed568b'), //maesa
    axios.get('https://api.waqi.info/feed/geo:16.433984;102.996006/?token=7fc8bbce26b91a5d2746cf445ae2920ee3ed568b'), //khonkaen
    axios.get('https://api.waqi.info/feed/geo:15.624560;100.190983/?token=7fc8bbce26b91a5d2746cf445ae2920ee3ed568b'), //nakornsawan
    axios.get('https://api.waqi.info/feed/geo:9.079848;99.335253/?token=7fc8bbce26b91a5d2746cf445ae2920ee3ed568b'), //surat
    axios.get('https://api.waqi.info/feed/geo:13.639952;100.414270/?token=7fc8bbce26b91a5d2746cf445ae2920ee3ed568b') //bangkok

  ]).then(axios.spread((maesa, khonkaen, nakornsawan, surat, bangkok) => {
    let arrayData = []
    
    arrayData.push(data)
    arrayData.push({
      gas: 0,
      smoke: 0,
      pm1: 0,
      pm2_5: ConcPM25(maesa.data.data.iaqi.pm25.v),
      pm10: ConcPM10(maesa.data.data.iaqi.pm10.v),
      latlon: maesa.data.data.city.geo,
      alias: 'maesa',
      place: maesa.data.data.city.name,
      pm2_5_aqi: maesa.data.data.aqi,
      humidity : isnan(maesa.data.data.iaqi.h),
      temperature : isnan(maesa.data.data.iaqi.t)
    })
    arrayData.push({
      gas: 0,
      smoke: 0,
      pm1: 0,
      pm2_5: ConcPM25(khonkaen.data.data.iaqi.pm25.v),
      pm10: ConcPM10(khonkaen.data.data.iaqi.pm10.v),
      latlon: khonkaen.data.data.city.geo,
      alias: 'khonkaen',
      place: khonkaen.data.data.city.name,
      pm2_5_aqi: khonkaen.data.data.aqi,
      humidity : isnan(khonkaen.data.data.iaqi.h),
      temperature : isnan(khonkaen.data.data.iaqi.t)
    })
    arrayData.push({
      gas: 0,
      smoke: 0,
      pm1: 0,
      pm2_5: ConcPM25(nakornsawan.data.data.iaqi.pm25.v),
      pm10: ConcPM10(nakornsawan.data.data.iaqi.pm10.v),
      latlon: nakornsawan.data.data.city.geo,
      alias: 'nakornsawan',
      place: nakornsawan.data.data.city.name,
      pm2_5_aqi: nakornsawan.data.data.aqi,
      humidity : isnan(nakornsawan.data.data.iaqi.h),
      temperature : isnan(nakornsawan.data.data.iaqi.t)
    })
    arrayData.push({
      gas: 0,
      smoke: 0,
      pm1: 0,
      pm2_5: ConcPM25(surat.data.data.iaqi.pm25.v),
      pm10: ConcPM10(surat.data.data.iaqi.pm10.v),
      latlon: surat.data.data.city.geo,
      alias: 'surat',
      place: surat.data.data.city.name,
      pm2_5_aqi: surat.data.data.aqi,
      humidity : isnan(surat.data.data.iaqi.h),
      temperature : isnan(surat.data.data.iaqi.t)
    })
    arrayData.push({
      gas: 0,
      smoke: 0,
      pm1: 0,
      pm2_5: ConcPM25(bangkok.data.data.iaqi.pm25.v),
      pm10: ConcPM10(bangkok.data.data.iaqi.pm10.v),
      latlon: bangkok.data.data.city.geo,
      alias: 'bangkok',
      place: bangkok.data.data.city.name,
      pm2_5_aqi: bangkok.data.data.aqi,
      humidity : isnan(bangkok.data.data.iaqi.h),
      temperature : isnan(bangkok.data.data.iaqi.t)
    })
    res.json(arrayData)
  }))
    .catch(function (error) {
      console.log(error)

    })

  
})

module.exports = router;
