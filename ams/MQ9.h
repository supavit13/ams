#include "Arduino.h"

class MQ9
{
public:
    MQ9(uint8_t in = A0);
    float value = 0.0;
    int raw = 0;
    uint8_t pin = A0;
    float rs_gas = 0.0;
    float ratio = 0.0;
    float rs_air = 0.0;
    float r0 = 0.0;
    int set_analog_input();
    void read_value();
    void read_gas();
    void caribration();
};
