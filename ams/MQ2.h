#include "Arduino.h"

class MQ2
{
public:
    MQ2(uint8_t in = A1);
    float value = 0.0;
    int raw = 0;
    uint8_t pin = A1;
    int set_analog_input();
    void read_value();
    float rs_air= 0;
    float r0= 0;
    float rs_gas= 0;
    float ratio= 0;
    void caribration();
    void read_smoke();
};
