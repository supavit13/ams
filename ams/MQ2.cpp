//caribration ref. >> http://wiki.seeedstudio.com/Grove-Gas_Sensor-MQ2/

#include "Arduino.h"
#include "MQ2.h"
MQ2::MQ2(uint8_t in = A1)
{
  pin = in;
  caribration();
}
int MQ2::set_analog_input()
{
  return analogRead(pin);
}
void MQ2::read_value()
{
  raw = set_analog_input();
  value = (float)raw / 1024 * 5.0;
}
void MQ2::caribration()
{
  float sensorVolt = 0;
  float sensorValue= 0;
  for (int i = 0; i < 100; i++)
  {
    sensorValue += (float)set_analog_input();
  }
  sensorValue = sensorValue / 100.0;
  sensorVolt = sensorValue / 1024.0 * 5.0;
  rs_air = (5.0 - sensorVolt) / sensorVolt;
  r0 = rs_air / 9.8; // The ratio of RS/R0 is 9.8 in a clear air from Graph (Found using WebPlotDigitizer)

}
void MQ2::read_smoke()
{
  float sensorVolt= 0;
  raw = set_analog_input();
  int sensorValue = raw;
  sensorVolt = (float)sensorValue / 1024.0 * 5.0;
  rs_gas = (5.0 - sensorVolt) / sensorVolt;
  ratio = rs_gas / r0;

}
