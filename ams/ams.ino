//sensor
#include "MQ9.h"
#include "MQ2.h"
#include "PMS.h"

#include "DHT.h"


#define DHTPIN 8
#define DHTTYPE DHT11
MQ9 gas;
MQ2 smoke;
PMS pms(Serial2);
PMS::DATA data;
DHT dht(DHTPIN, DHTTYPE);
#define GASPIN A3
#define SMOKEPIN A11

//watchdog
#include <avr/wdt.h>

//internet
#include "TEE_UC20.h"
#include "internet.h"
#include "uc_mqtt.h"
INTERNET net;
UCxMQTT mqtt;
/*       APN       */
#define APN "internet"
#define USER ""
#define PASS ""

/*       MQTT information       */
#define MQTT_SERVER      "soar.gistda.or.th"
#define MQTT_PORT        "1883"
char * MQTT_ID = "clientId-";
#define MQTT_USER        " "
#define MQTT_PASSWORD    " "
#define MQTT_WILL_TOPIC    0
#define MQTT_WILL_QOS      0
#define MQTT_WILL_RETAIN   0
#define MQTT_WILL_MESSAGE  0

/*       Device information       */
#define ALIAS "soar" //device name
#define PLACE "Space Krenovation Park" //device location place name
String location = "13.102115,100.927842"; //latitude,longitude

#define randomSeedPin A4

unsigned long previousmqtt = 0;
const long intervalmqtt = 1000;
void debug(String data)
{
  Serial.println(data);
}
void initMqttClientId()
{
  randomSeed(analogRead(randomSeedPin));
  String randomId = "clientId-" + String(random(999, 1000000));
  randomId.toCharArray(MQTT_ID, randomId.length());

}
void serialFlush()
{
  Serial.flush();
  Serial1.flush();
  Serial2.flush();
}
void startWatchdog(String msg){
  Serial.println(msg);
  wdt_enable(WDTO_8S);
}
void endWatchdog(String msg){
  wdt_reset();
  wdt_disable();
  Serial.println(msg);
}
void setup() {
  
  Serial.begin(9600);
  Serial.print("Arduino rebooted");
  analogWrite(A0,255);
  analogWrite(A1,0);
  analogWrite(A2,0);
  analogWrite(A8,255);
  analogWrite(A9,0);
  analogWrite(A10,0);
  pinMode(9, OUTPUT);
  digitalWrite(9,HIGH);
  pinMode(10,OUTPUT);
  digitalWrite(10,LOW);
  
  gas = MQ9(GASPIN);
  smoke = MQ2(SMOKEPIN);
  dht.begin();
  Serial2.begin(9600); //17(RX), 16(TX)
  initMqttClientId();
  Serial.print("MQTT_ID : ");
  Serial.println(MQTT_ID);
  startWatchdog("watchdog for gsm power on process. it will reset when do code below exceed 8 seconds.");
  gsm.begin(&Serial1, 9600);
  gsm.Event_debug = debug;
  Serial.println(F("UC20"));
  gsm.PowerOn();
  endWatchdog("watchdog end. because do code above less than 8 seconds");
  
  while (gsm.WaitReady()) {}
  Serial.print(F("GetOperator --> "));
  Serial.println(gsm.GetOperator());
  Serial.print(F("SignalQuality --> "));
  Serial.println(gsm.SignalQuality());

  Serial.println(F("Disconnect net"));
  net.DisConnect();
  Serial.println(F("Set APN and Password"));
  net.Configure(APN, USER, PASS);
  Serial.println(F("Connect net"));
  net.Connect();
  Serial.println(F("Show My IP"));
  Serial.println(net.GetIP());
  mqtt.callback = callback;
  connect_server();
  
}

void callback(String topic , char * payload, unsigned char length)
{
  Serial.println();
  Serial.println(F("%%%%%%%%%%%%%%%%%%%%%%%%%%%%"));
  Serial.print(F("Topic --> "));
  Serial.println(topic);
  payload[length] = 0;
  String str_data(payload);
  Serial.print(F("payload --> "));
  Serial.println(str_data);
}

void connect_server()
{
  do
  {
    Serial.println(F("Connect Server"));
    Serial.println(F("wait connect"));
    if (mqtt.DisconnectMQTTServer())
    {
      mqtt.ConnectMQTTServer(MQTT_SERVER, MQTT_PORT);
    }
    delay(500);
    Serial.println(mqtt.ConnectState());
  }
  while (!mqtt.ConnectState());
  Serial.println(F("Server Connected"));
  unsigned char ret = mqtt.Connect(MQTT_ID, MQTT_USER, MQTT_PASSWORD);
  Serial.println(mqtt.ConnectReturnCode(ret));
  mqtt.Publish("ams", "hello world", false);
  mqtt.Subscribe("inTopic");
}
void loop() {
  wdt_enable(WDTO_8S);
  
  gas.read_value();
  //  gas.read_gas();
  smoke.read_value();
  //  smoke.read_smoke();

  if (pms.read(data))
  {
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    if (isnan(h) || isnan(t)) {
      Serial.println(F("Failed to read from DHT sensor!"));
    }
    Serial.print("Humidity: ");
    Serial.println(h);

    Serial.print("Temperature: ");
    Serial.println(t);

    Serial.print("PM 1.0 (ug/m3): ");
    Serial.println(data.PM_AE_UG_1_0);

    Serial.print("PM 2.5 (ug/m3): ");
    Serial.println(data.PM_AE_UG_2_5);

    Serial.print("PM 10.0 (ug/m3): ");
    Serial.println(data.PM_AE_UG_10_0);

    Serial.println();
    String payload = String(gas.raw) + "," + String(smoke.raw) + "," + String(data.PM_AE_UG_1_0) + "," + String(data.PM_AE_UG_2_5) + "," + String(data.PM_AE_UG_10_0) + "," + location + "," + ALIAS + "," + PLACE + ","+String(h)+","+String(t)+",";
    Serial.println(payload);
    mqtt.Publish("ams", payload, false);
    serialFlush();
  }
  unsigned long currentMillis = millis();
  if (currentMillis - previousmqtt >= intervalmqtt)
  {
    previousmqtt = currentMillis;
    if (mqtt.ConnectState() == false)
    {
      Serial.println(F("Reconnect"));
      connect_server();
    }
  }
  wdt_reset();
  mqtt.MqttLoop();
  //  delay(1000);
}
