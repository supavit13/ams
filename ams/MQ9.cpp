//caribration ref. >> http://wiki.seeedstudio.com/Grove-Gas_Sensor-MQ9/
#include "Arduino.h"
#include "MQ9.h"
MQ9::MQ9(uint8_t in = A0)
{
  pin = in;
  caribration();
}
int MQ9::set_analog_input()
{
  return analogRead(pin);
}
void MQ9::read_value()
{
  raw = set_analog_input();
  value = (float)raw / 1024 * 5.0;
}
void MQ9::caribration()
{
  float sensorValue = 0.0;
  float sensorVolt = 0.0;
  for (int i = 0; i < 100; i++)
  {
    sensorValue += (float)set_analog_input();
  }
  sensorValue = sensorValue / 100.0;

  sensorVolt = sensorValue / 1024.0 * 5.0;
  rs_air = (5.0 - sensorVolt) / sensorVolt;
  r0 = rs_air / 9.9; // The ratio of RS/R0 is 9.9 in LPG gas from Graph (Found using WebPlotDigitizer)
}
void MQ9::read_gas()
{
  float sensorValue = 0.0;
  float sensorVolt = 0.0;
  raw = set_analog_input();
  sensorValue = (float)raw;
  sensorVolt = (float)sensorValue / 1024.0 * 5.0;
  rs_gas = (5.0 - sensorVolt) / sensorVolt;
  ratio = rs_gas / r0;

}
